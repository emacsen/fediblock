This package contains a prototype for an DNSBL/RBL style blasklist meant for ActivityPub implementations.  *THIS IS NOT PRODUCTION SOFTWARE*

# Installation/Running

This prootype runs very simply. You need Python and Poetry. Then Just run `poetry install` and `./run.py`

For the DNS portion, you will need a DNS server such as `rbldnsd`. The file generated from `/rdbl` can be fed into it as a zonefile.

# FAQ

## What is an DNSBL/RBL?

When an SMTP server (Alice) connects to another email server (Bob), Bob will want to know if Alice is a known spammer. To do that, he checks against trusted email spam blocklists, let's call it Charlie.

The mechanism that Bob uses is that he makes a DNS query

## Why DNS?

DNS is excellent for this purpose because DNS queries are fast, cachable and delgatable. Unlike an HTTP request that is TCP-based, a DNS query is a single UDP packet and a single UDP return. In addition, the response can be cached along the way, meaning that the original DNS server (Charlie) may never even see the request. Lastly, DNS responses have a caching value (TTL) which lets the requester know how long to keep the response.

## So I make a DNS query?

Yes. Let's imagiene the DNS server is at rbl.example.com, the query would be <hostname>.rbl.example.com and we wanted to check `hatespeech.social`, we would ask our DNS server for `hatespeech.social.rbl.example.com`. Not that we don't need to query rbl.example.com ourselves, DNS will take care of that for us.

## What are the values supported?

DNSBLs use known IP addresses as response codes, explained in RFC 5782 ( https://datatracker.ietf.org/doc/html/rfc5782 ), this code uses the same principle, with the main IP adress being 127.0.0.22, indicating that this instance has been part of a targeted harassment campaign or actively supports hate speech.


| IP Address | Description                                                 |
|------------|-------------------------------------------------------------|
| 127.0.0.20 | Instance is misconfigured                                   |
| 127.0.0.21 | Instance is non-moderated/does not respond to reports       |
| 127.0.0.22 | Instance is used for active harassment/supports hate speech |

Additional identifiers may be added later (this is only a prototype!)

These values are returned as A records, but their textual values may be returned as TXT records as well.

## But I don't like DNS!

That's fine! This code has a web server which returns RESTful results. Just query the hostname at `/lookup` and get back a REST response like:

```
{ "name": "hatespeech.social",
  "matching": "exact",
   "reason": "Too many complaints of hate speech"
}
```

A 404 incidates no record, which is a good thing in this case.

