from typing import Optional, NamedTuple
from fastapi import FastAPI, HTTPException, Header, Response

# This is a simple textfile but in the future it will either be a CSV
# or a database
BLOCKLIST = "blocklist.txt"

# Block reasons
MISCONFIGURED = '127.0.0.20'
NOMODERATION = '127.0.0.21'
HARASSMENT = '127.0.0.22'

app = FastAPI()

class Record(NamedTuple):
    """A Record in the blocklist"""
    name: str
    # Default behvior is exact but maybe it should be inclusive
    matching: str = 'exact'
    reason: str = ''
    
    def __repr__(self) -> str:
        return f'<Record {self.name}>'

# In this prototype, we don't even use a database, just a simple textfile!
def read_blocklist():
    servers = {}
    with open(BLOCKLIST, 'r') as fd:
        for line in fd:
            line = line.strip()
            if not line or line[0] == '#':
                continue
            instance = line.split('#')[0].strip().lower()
            if instance.startswith('.*'):
                name = instance[2:]
                matching = 'subdomains'
            elif instance.startswith('.'):
                name = instance[1:]
                matching = 'inclusive'
            else:
                name = instance
                matching = 'exact'
            servers[name] = Record(name=name, matching=matching)
    return servers

blocklist = read_blocklist()

def rdbl_entry(record):
    """Takes a record and returns an RDBL compatible entry"""
    reasons = {'exact': '',
               'subdomains': '.*',
               'inclusive': '.'}
    return f"{reasons.get(record.matching, '')}{record.name} :{HARASSMENT}"

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.get("/lookup")
def read_lookup(id: str):
    # Try to identify the domain up to domain.tld
    lookup = id
    while lookup.split('.') > 2:
        if matching := blocklist.get(lookup):
            if matching == 'exact':
                if lookup == id:
                    return {'domain': id, 'blocked': True}
                else:
                    raise HTTPException(status_code=404, detail="Exact match not found")
            elif matching == 'subdomains':
                if lookup != id:
                    return {'domain': id, blocked: True}
                else:
                    raise HTTPException(status_code=404, detail="Exact domain does not match")
            elif matching == 'inclusive':
                return {'domain': id, 'blocked': True}
        lookup = lookup.split('.')[1:].join('.')

    # Now do a final check through
    if matching := blocklist.get(id):
        if matching == 'exact' or matching == 'inclusive':
            return {'domain': id, 'blocked': True}
        elif matching == 'subdomains':
            raise HTTPException(status_code=404, detail="Exact domain does not match")

    # Otherwise not found
    raise HTTPException(status_code=404, detail="Instance not found")

@app.get("/blocklist")
def read_blocklist():
#    return {"blocklist": sorted(list(blocklist))}
    return {"blocklist": [r._asdict() for r in blocklist.values()]}

@app.get('/rdbl')
def read_rdbl():
    """Return an RDBL compatible blocklist"""
    # If the list becomes larger, we can stream it, but constructing
    # it in place is fine for now
    #
    # Make a long string containing the data
    data = '\n'.join([rdbl_entry(r) for r in blocklist.values()]) + '\n'
    return Response(content=data, media_type="application/dns")
