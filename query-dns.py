#!/usr/bin/env python
from sys import argv, exit
from os import argv
from socket import gethostbyname

RBL_DOMAIN = 'rbl.example.com'

# Block reasons
MISCONFIGURED = '127.0.0.20'
NOMODERATION = '127.0.0.21'
HARASSMENT = '127.0.0.22'

HOST = argv[1] + '.' + RBL_DOMAIN

ip = gethostbyname(HOST)

try:
    ip = gethostbyname(HOST)
except socket.gaierror:
    exit(0)

if ip == MISCONFIGURED:
    print("Misconfigured")
if ip == NOMODERATION:
    print("No moderation")
if ip == HARASSMENT:
    print("Targeted harassment/hate speech")

exit(1)
