#!/usr/bin/env python
from sys import argv, exit
import requests

HOST = argv[1]

WEBSERVER = 'http://localhost:8000'

response = requests.get(WEBSERVER + "/lookup", params={'id': HOST})
if response.status_code == 404:
    exit(0)
elif response.status_code = 200:
    json = response.json()
    # We could parse this and get the reaso...
    print("Blocked")
    exit(1)
else:
    exit(2)
